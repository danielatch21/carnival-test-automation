import { Task, PerformsTasks } from "serenity-js/lib/screenplay";
import { Enter, Click } from "serenity-js/lib/serenity-protractor";
import { login } from "../../ui/login/login";
import { Pause } from "../../interactions/pause";

//Aquí se definen las interacciones con cada uno de los elementos de la página Ej: Escribir, hacer clic
export class ClickLogInLink implements Task{
    performAs(actor: PerformsTasks){
        return actor.attemptsTo(
            Click.on(login.Login_Link)
        )
    }
    constructor(){}    
}

export class EnterUsername implements Task{
    performAs(actor: PerformsTasks){
        return actor.attemptsTo(
            Enter.theValue(this.username).into(login.Username_Field)
        )
    }
    constructor(private username:string){}
}

export class EnterPassword implements Task{
    performAs(actor: PerformsTasks){
        return actor.attemptsTo(
            Enter.theValue(this.password).into(login.Password_Field)
        )
    }
    constructor(private password:string){}
}

export class ClickLogInButton implements Task{
    performAs(actor: PerformsTasks){
        return actor.attemptsTo(
            Click.on(login.LogIn_Button)
        )
    }
    constructor(){}    
}

export class EnterCredentials implements Task{

    static as(username: string, password: string){
        return new EnterCredentials(username, password)
        
    }
    performAs(actor:PerformsTasks){
        return actor.attemptsTo(
            new ClickLogInLink(),
            new EnterUsername(this.username),
            new EnterPassword(this.password),
            Pause.for(1000),
            new ClickLogInButton(),
            Pause.for(1000)
        )
    }
    constructor(private username:string, private password:string){}
}