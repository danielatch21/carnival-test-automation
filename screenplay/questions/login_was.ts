import { Task, See } from "serenity-js/lib/screenplay";
import { Wait, Is, Text } from "serenity-js/lib/serenity-protractor";
import { analysis } from "../ui/analysis/analysis";
import { include } from "../../expect";


//En este método se valida cuando el login no fue satisfactorio
export const LoginWasWrong = () => Task.where('#actor should see error message',
    Wait.until(analysis.Unsuccessful_Login, Is.visible()),
    See.if(Text.of(analysis.Unsuccessful_Login),include("We're sorry but the credentials entered do not match."))
)
//En este método se valida los mensajes del campo username cuando lo envian vacio
export const UsernameRequired = () => Task.where('#actor should see alert about the username required',
    Wait.until(analysis.Username_Required, Is.visible()),
    See.if(Text.of(analysis.Username_Required),include("E-mail/username is required"))
)
//En este método se valida los mensajes del campo password cuando lo envian vacio
export const PasswordRequired = () => Task.where('#actor should see alert about the password required',
    Wait.until(analysis.Password_Required, Is.visible()),
    See.if(Text.of(analysis.Password_Required),include("Password is required"))
)