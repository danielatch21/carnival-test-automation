import { Target } from "serenity-js/lib/serenity-protractor";
import { by } from "protractor";

export const login = {
    //Aquí se definen los localizadores para cada uno de los elementos con los que se requiere interactuar
    Login_Link: Target.the('"Login link"').located(by.id("ccl_header_expand-login-link")),
    Username_Field: Target.the('"Username field"').located(by.id("username")),
    Password_Field: Target.the('"Password field"').located(by.id("password")),
    LogIn_Button: Target.the('"Log in button"').located(by.className("lrc-submit-button cclr-button-secondary cclr-button-small"))
}