Feature: User is able to navigate through the application

  In order to focus on things that matter
  Mario would like to navigate through the application
  to validate it is working as expected

Scenario: Unsuccesful login

Given that Mario opens the Login page
When he enters a wrong credentials
Then he should see alert about the invalid credentials

Scenario: Required fields login

Given that Jesus opens the Login page
When he does not enters credentials
Then he should see alert about the required fields