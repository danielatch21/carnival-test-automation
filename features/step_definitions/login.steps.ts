import { Navigate } from "../../screenplay/tasks/navigate/navigate";
import { EnterCredentials } from "../../screenplay/tasks/login/enter_credentials";
import { LoginWasWrong, UsernameRequired, PasswordRequired } from "../../screenplay/questions/login_was";

export = function loginSteps(){
    //Validación de credenciales inválidas
    this.Given(/^that Mario opens the Login page$/, function () {
        return this.stage.theActorCalled('Mario').attemptsTo(
            Navigate.toCarnival()
        )   
    });

    this.When(/^he enters a wrong credentials$/, function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            EnterCredentials.as('MARIO2', 'Asdf1234')
        )   
    });

    this.Then(/^he should see alert about the invalid credentials$/, function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            LoginWasWrong()
        )           
    });

    //Validación campos obligatorios
    this.Given(/^that Jesus opens the Login page$/, function () {
        return this.stage.theActorCalled('Jesus').attemptsTo(
            Navigate.toCarnival()
        )   
    });

    this.When(/^he does not enters credentials$/, function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            EnterCredentials.as('', '')
        )   
    });

    this.Then(/^he should see alert about the required fields$/, function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            UsernameRequired(),
            PasswordRequired()
        )           
    });

    

}