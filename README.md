# Prueba de automatización Login Carnival

## ¿Que contiene este repositorio?
This repository contains test automation for login page from carnival (Home page: http://www.carnival.com).

## ¿Con quién se puede hablar acerca de este repositorio?
Ingeniera de calidad de software: Daniela Tobón

## Requerimientos para ejecutar los test:

1. Clonar este repositorio.
2. Tener NPM instalado en la versión: 6.4.1
3. Para instalar los paquetes necesarios ejecutar npm install

## Dependencias requeridas para la ejecución de los test:
```
"devDependencies": {
    "cucumber": "^1.3.2",
    "protractor": "^5.4.1",
    "serenity-cli": "^0.11.1",
    "serenity-js": "^1.10.13",
    "ts-node": "^7.0.1",
    "typescript": "^3.2.1"
  }
  
"dependencies": {
    "chai": "^4.1.2",
    "chai-as-promised": "^7.1.1",
    "chai-as-promised-compat": "^7.0.3"
  }
```
Para ejecutar los test se debe ejecutar: npm run test

El resultado de los test se pueden visualizar en la consola desde dónde se ponen a correr.